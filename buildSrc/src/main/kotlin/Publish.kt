object Publish {
    const val GROUP = "com.pajato.ctci"
    const val USER = "pajato"
    const val POM_ARTIFACT_ID = "ctci-utils"
    const val POM_DESCRIPTION = "CtCI utility classes and functions provided in Kotlin"
    const val POM_DEVELOPER_ID = "pajatopmr"
    const val POM_DEVELOPER_NAME = "Paul Michael Reilly"
    const val POM_ORGANIZATION_NAME = "Pajato Technologies LLC"
    const val POM_ORGANIZATION_URL = "https://pajato.com/"
    const val POM_NAME = POM_ARTIFACT_ID
    const val POM_LICENSE_DIST = "repo"
    const val POM_LICENSE_NAME = "The GNU General Public License, Version 3"
    const val POM_LICENSE_URL = "https://gnu.org/licenses/gpl-3.0.html"
    const val POM_SCM_CONNECTION = "scm:git:https://gitlab.com/pmr-ctci-boston/kotlin/ctci-utils.git"
    const val POM_SCM_DEV_CONNECTION = "scm:git:git@gitlab.com:pmr/pmr-ctci-boston/kotlin/ctci-utils.git"
    const val POM_SCM_URL = "https://gitlab.com/pmr-ctci-boston/kotlin/ctci-utils"
    const val POM_URL = "https://github.com/pmr-ctci-boston/kotlin/ctci-utils"
}
