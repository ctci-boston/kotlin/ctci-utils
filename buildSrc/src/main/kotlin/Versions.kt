// SPDX-License-Identifier: LGPL-3.0-or-later

object Versions {
    const val KOTLIN = "1.3.50"
    const val CTCI_UTILS = "4.1"
}
