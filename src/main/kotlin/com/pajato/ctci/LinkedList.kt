package com.pajato.ctci

data class Node<T>(var value: T, var next: Node<T>? = null) {
    fun length(): Int {
        tailrec fun length(current: Node<T>?, size: Int): Int {
            if (current == null) return size
            return length(current.next, size + 1)
        }

        return length(this, 0)
    }

    fun tail(): Node<T> {
        tailrec fun tail(current: Node<T>): Node<T> {
            val next = current.next ?: return current
            return tail(next)
        }

        return tail(this)
    }

    override fun toString(): String = if (hasLoop()) "Circular list detected!" else super.toString()

    private fun hasLoop(): Boolean {
        // Use the runner pattern to detect a circular (corrupted) loop.
        tailrec fun hasLoop(slow: Node<T>?, fast: Node<T>?): Boolean {
            if (slow === fast) return true
            val nextFast = fast?.next?.next ?: return false
            return hasLoop(slow?.next, nextFast)
        }

        return if (this == this.next) true else hasLoop(this, this.next?.next)
    }
}

fun <T> createLinkedList(vararg values: T): Node<T>? {
    tailrec fun createLinkedList(current: Node<T>, index: Int) {
        if (index >= values.size) return
        val next = Node(values[index])
        current.next = next
        createLinkedList(next, index + 1)
    }

    if (values.isEmpty()) return null
    val head = Node(values[0])
    createLinkedList(head, 1)
    return head
}
