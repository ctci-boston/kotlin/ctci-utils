package com.pajato.ctci

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class CtCITest {

    @Test
    fun `verify an empty linked list can be created`() {
        assertEquals(null, createLinkedList<Int>())
    }

    @Test fun `verify that a linked list with one node can be created`() {
        val expected = Node(0)
        val actual = createLinkedList(0)
        assertEquals(expected.value, actual?.value ?: fail("The actual node is null!"))
        assertEquals(expected.next, actual.next)
        assertEquals(1, actual.length())
    }

    @Test fun `verify a list with two nodes can be created`() {
        val expected = Node(1)
        val expectedNext = Node(2)
        expected.next = expectedNext

        val actual = createLinkedList(1, 2)
        val actualNext = actual?.next
        assertEquals(expected.value, actual?.value ?: fail("The actual node is null!"))
        assertEquals(expectedNext.value, actual.next?.value ?: fail("Second node is wrong!"))
        assertEquals(null, actualNext?.next)
        assertEquals(2, actual.length())
    }

    @Test fun `verify a long list can be created correctly`() {
        val actual = createLinkedList(1, 2, 3, 4, 5, 6, 7, 8)
        assertEquals(1, actual?.value ?: fail("The first node is null!"))
        assertEquals(2, actual.next?.value ?: fail("The second node is null!"))
        assertEquals(3, actual.next?.next?.value ?: fail("The third node is null!"))
        assertEquals(4, actual.next?.next?.next?.value ?: fail("The fourth node is null!"))
        assertEquals(5, actual.next?.next?.next?.next?.value ?: fail("The fifth node is null!"))
        assertEquals(6, actual.next?.next?.next?.next?.next?.value ?: fail("The sixth node is null!"))
        assertEquals(7, actual.next?.next?.next?.next?.next?.next?.value ?: fail("The seventh node is null!"))
        assertEquals(8, actual.next?.next?.next?.next?.next?.next?.next?.value ?: fail("The eighth node is null!"))
        val tail = actual.next?.next?.next?.next?.next?.next?.next ?: fail("the list does not terminate correctly!")
        assertEquals(tail.next, null)
        assertEquals(8, actual.length())
    }

    @Test fun `verify two identical long lists are equal`() {
        val list1 = createLinkedList(1, 2, 3, 4, 5, 6, 7)
        val list2 = createLinkedList(1, 2, 3, 4, 5, 6, 7)
        assertTrue(list1?.equals(list2) ?: fail("Invalid list!"))
    }

    @Test fun `verify two different long lists are not equal`() {
        val list1 = createLinkedList(1, 2, 3, 4, 5, 6, 8)
        val list2 = createLinkedList(1, 2, 3, 4, 5, 6, 7)
        assertFalse(list1?.equals(list2) ?: fail("Invalid list!"))
    }

    @Test fun `verify the tail of a single node is itself`() {
        val node = Node('a')
        assertEquals(node.tail(), node)
    }

    @Test fun `verify the tail of a two node linked list is the second node`() {
        val node = Node('a')
        val tail = Node('b')
        node.next = tail
        assertEquals(tail, node.tail())
    }

    @Test fun `verify that a non-looping list is detected`() {
        assertTrue(Node(1).toString() != "Circular list detected!")
    }

    @Test fun `verify that a looping list is detected`() {
        val head = Node(1)
        head.next = head
        assertEquals("Circular list detected!", Node(1).toString())
    }
}
