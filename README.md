# CtCI Utility Classes and Functions

This project provides utility classes and functions used by the various chapters such as class Node and the function createLinkedList.

To use these resources, clone the project and install the artifacts to the local maven repository. This will allow the projects using these artifacts to find them.
